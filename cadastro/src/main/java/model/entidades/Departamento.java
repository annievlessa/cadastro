package model.entidades;

public class Departamento {

	private Integer idDepartamento;
    private String name;
    
	public Integer getIdDepartamento() {
		return idDepartamento;
	}
	
	public void setIdDepartamento(Integer idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
}