package model.dao;

import java.io.IOException;
import java.sql.SQLException;

import db.Db;

public class DaoFactory {

	public static VendedorDao criarVendedorDao(){
        return null; // new VendedorDaoJDBC(Db.getConnection());
    }

    public static DepartamentoDao criarDepartamentoDao() throws SQLException, IOException{
        return new ImplDepartamentoDao(Db.getConnection());
    }
	
}
