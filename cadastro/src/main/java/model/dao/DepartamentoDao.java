package model.dao;

import java.sql.SQLException;
import java.util.List;

import model.entidades.Departamento;

public interface DepartamentoDao {
	
	public void incluir(Departamento departamento) throws SQLException;
    
	public void atualizar(Departamento departamento) throws SQLException;
    
	public void deletarPorId(Integer id) throws SQLException;
    
	public Departamento consultarPorId(Integer id) throws SQLException;
    
	public List<Departamento> listarDepartamento() throws SQLException;

}
