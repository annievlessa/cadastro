package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.Db;
import model.entidades.Departamento;

public class ImplDepartamentoDao implements DepartamentoDao {

	private Connection conn;

    public ImplDepartamentoDao(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void incluir(Departamento departamento) throws SQLException {
    	
        PreparedStatement st = null;

        try {
        	
            st = conn.prepareStatement("INSERT INTO departamento (nome) VALUES (?)", Statement.RETURN_GENERATED_KEYS);

            st.setString(1, departamento.getName());

            int rowsAffected = st.executeUpdate();

            if (rowsAffected > 0){
            	
                ResultSet rs = st.getGeneratedKeys();
                
                if (rs.next()){
                    int id = rs.getInt(1);
                    departamento.setIdDepartamento(id);
                }
                
                Db.closeResultSet(rs);
                
            }
            
        }catch (SQLException e){
            throw e;
        }finally {
            Db.closeStatement(st);
        }
        
    }

    @Override
    public void atualizar(Departamento obj) throws SQLException {
    	
        PreparedStatement st = null;

        try {
        	
            st = conn.prepareStatement("UPDATE departamento SET Name = ? WHERE Id = ?");

            st.setString(1, obj.getName());
            st.setInt(2, obj.getIdDepartamento());

            st.executeUpdate();
            
        }catch (SQLException e){
            throw e;
        }finally {
            Db.closeStatement(st);
        }
        
    }

    @Override
    public void deletarPorId(Integer id) throws SQLException {
    	
        PreparedStatement st = null;

        try {
        	
            st = conn.prepareStatement("DELETE FROM departamento WHERE Id = ?");
            
            st.setInt(1, id);
            st.executeUpdate();
            
        }catch (SQLException e){
            throw e;
        }finally {
            Db.closeStatement(st);
        }
        
    }

    @Override
    public Departamento consultarPorId(Integer id) throws SQLException {
    	
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
        	
            st = conn.prepareStatement("SELECT * FROM departamento WHERE Id = ?");

            st.setInt(1, id);
            rs = st.executeQuery();

            if(rs.next()){
                Departamento dep = new Departamento();
                dep.setIdDepartamento(id);
                dep.setName(rs.getString("Name"));

                return dep;
            }
            
            return null;
            
        }catch (SQLException e){
            throw e;
        }finally {
            Db.closeStatement(st);
            Db.closeResultSet(rs);
        }
        
    }

    @Override
    public List<Departamento> listarDepartamento() throws SQLException {
    	
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
        	
            st = conn.prepareStatement("SELECT * FROM department ORDER BY Name");

            rs = st.executeQuery();

            List<Departamento> departments = new ArrayList<>();

            while (rs.next()){
            	Departamento dep = new Departamento();
                dep.setIdDepartamento(rs.getInt("Id"));
                dep.setName(rs.getString("Name"));

                departments.add(dep);
            }

            return departments;
            
        }catch (SQLException e){
            throw e;
        }finally {
            Db.closeStatement(st);
            Db.closeResultSet(rs);
        }
        
    }	
	
}