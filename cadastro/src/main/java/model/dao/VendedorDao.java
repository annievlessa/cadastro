package model.dao;

import java.util.List;

import model.entidades.Departamento;
import model.entidades.Vendedor;

public interface VendedorDao {

	void incluir(Vendedor obj);
    void alterar(Vendedor obj);
    void deletarPorId(Integer id);
    Vendedor consultarPorId(Integer id);
    List<Vendedor> listarVendedor();

    List<Vendedor> listarPorDepartamento(Departamento departamento);
    
}